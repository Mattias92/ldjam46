﻿using LDJam46.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Components
{
    [Serializable()]
    public class Component
    {
        public Entity Owner;
    }
}
