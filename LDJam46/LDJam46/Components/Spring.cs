﻿using LDJam46.World;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Components
{
    [Serializable()]
    public class Spring : Component
    {
        private float _directionX;
        private float _directionY;
        public Vector2 Direction
        {
            get
            {
                return new Vector2(_directionX, _directionY);
            }
            set
            {
                _directionX = value.X;
                _directionY = value.Y;
            }
        }

        public float MaxDistance = 100;
        private float _distance;
        public float Distance
        {
            get
            {
                return _distance;
            }
            set
            {
                _distance = value;
                if (_distance > MaxDistance)
                {
                    _distance = MaxDistance;
                }
            }
        }

        public int RechargeTimer = 0;

        public Spring()
        {

        }

        public Spring(Entity owner, Vector2 direction, float distance)
        {
            Owner = owner;
            Direction = direction;
            Distance = distance;
        }
    }
}
