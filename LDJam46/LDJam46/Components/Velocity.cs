﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Components
{
    [Serializable()]
    public class Velocity : Component
    {
        public float X = 0f;
        public float Y = 0f;
        public Vector2 Vector2
        {
            get
            {
                return new Vector2(X, Y);
            }
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }
        public bool HasGravity = true;
        public bool GotHitByRay = false;
    }
}
