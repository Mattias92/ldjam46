﻿using LDJam46.Physics;
using LDJam46.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Components
{
    [Serializable()]
    public class BoxCollider : Component
    {
        public List<Entity> Collisions;
        public AABB AABB;

        public BoxCollider(Entity owner, float x, float y, float width, float height)
        {
            Collisions = new List<Entity>();
            Owner = owner;
            AABB = new AABB(x, y, width, height);
        }

        public BoxCollider()
        {

        }
    }
}
