﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Components
{
    [Serializable()]
    public class Sprite : Component
    {
        public int Width;
        public int Height;
        public float Rotation;
        public string TexturePath;
        [NonSerialized()]
        public Texture2D Texture;
        [NonSerialized()]
        public Color Color;
        public float Order = 1f;

        private float _originX = 0;
        private float _originY = 0;
        public Vector2 Origin
        {
            get
            {
                return new Vector2(_originX, _originY);
            }
            set
            {
                _originX = value.X;
                _originY = value.Y;
            }
        }

        public Sprite(ContentManager contentManager, string texturePath, int width, int height)
        {
            Width = width;
            Height = height;
            Rotation = 0f;
            TexturePath = texturePath;
            Texture = contentManager.Load<Texture2D>(texturePath);
        }

        public Sprite()
        {

        }
    }
}
