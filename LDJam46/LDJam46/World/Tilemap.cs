﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.World
{
    [Serializable()]
    public class Tilemap
    {
        public int Width;
        public int Height;

        private Tile[][] tiles;
        private string atlasPath;
        [NonSerialized()]
        private Texture2D atlas;

        public Tilemap(ContentManager contentManager, string atlasPath, int width, int height)
        {
            Width = width;
            Height = height;
            tiles = new Tile[width][];
            this.atlasPath = atlasPath;
            atlas = contentManager.Load<Texture2D>(atlasPath);

            for (int x = 0; x < width; x++)
            {
                tiles[x] = new Tile[height];

                for (int y = 0; y < height; y++)
                {
                    tiles[x][y] = new Tile(false, 0, 0);
                }
            }
        }

        public Tile GetTile(int x, int y)
        {
            if (x >= 0 && x < Width && y >= 0 && y < Height)
            {
                return tiles[x][y];
            }
            return null;
        }

        public void SetTile(int x, int y, Tile tile)
        {
            if (x >= 0 && x < Width && y >= 0 && y < Height)
            {
                tiles[x][y].U = tile.U;
                tiles[x][y].V = tile.V;
                tiles[x][y].IsCollidable = tile.IsCollidable;
            }
        }

        public bool IsCollidable(Vector2 position)
        {
            int x = (int)position.X / 32;
            int y = (int)position.Y / 32;

            if (x >= 0 && x < Width && y >= 0 && y < Height)
            {
                return tiles[x][y].IsCollidable;
            }
            return false;
        }

        public bool IsCollidable(int x, int y)
        {
            if (x >= 0 && x < Width && y >= 0 && y < Height)
            {
                return tiles[x][y].IsCollidable;
            }
            return false;
        }

        public void OnLoad(ContentManager contentManager)
        {
            atlasPath = "grassTiles3";
            atlas = contentManager.Load<Texture2D>(atlasPath);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    Tile tile = tiles[x][y];
                    spriteBatch.Draw(atlas, new Vector2(x * Tile.Size, y * Tile.Size), new Rectangle(tile.U * Tile.Size, tile.V * Tile.Size, Tile.Size, Tile.Size), Color.White);
                }
            }
            spriteBatch.End();
        }
    }
}
