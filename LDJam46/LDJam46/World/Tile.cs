﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.World
{
    [Serializable()]
    public class Tile
    {
        public const int Size = 32;
        public bool IsCollidable;
        public int U;
        public int V;

        public Tile(bool isCollidable, int u, int v)
        {
            IsCollidable = isCollidable;
            U = u;
            V = v;
        }
    }
}
