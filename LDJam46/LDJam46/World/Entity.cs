﻿using LDJam46.Components;
using LDJam46.Physics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.World
{
    [Serializable()]
    public class Entity
    {
        public int Id;
        public bool IsKilled = false;

        private List<Component> components;
        private Dictionary<Type, Component> componentsDictionary;

        public float X;
        public float Y;
        public Vector2 Position
        {
            get
            {
                return new Vector2(X, Y);
            }
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }

        public Entity(int id, Vector2 position)
        {
            Id = id;
            Position = position;
            components = new List<Component>();
            componentsDictionary = new Dictionary<Type, Component>();
        }

        public Entity(int id, Vector2 position, Entity template)
        {
            Id = id;
            Position = position;

            components = new List<Component>();
            componentsDictionary = new Dictionary<Type, Component>();

            foreach (Component tc in template.GetAllComponents())
            {
                Component c = (Component)Activator.CreateInstance(tc.GetType());
                foreach (var fieldInfo in tc.GetType().GetFields())
                {
                    object tcValue = fieldInfo.GetValue(tc);
                    fieldInfo.SetValue(c, tcValue);
                }

                c.Owner = this;
                if (c is BoxCollider)
                {
                    BoxCollider boxCollider = (BoxCollider)c;
                    boxCollider.AABB = new AABB(boxCollider.AABB);
                }

                AddComponent(c);
            }
        }

        public void AddComponent(Component component)
        {
            if (!ContainsComponent(component))
            {
                components.Add(component);
                componentsDictionary.Add(component.GetType(), component);
            }
        }

        public bool ContainsComponent<T>() where T : Component
        {
            return componentsDictionary.ContainsKey(typeof(T));
        }

        public bool ContainsComponent(Component component)
        {
            return componentsDictionary.ContainsKey(component.GetType());
        }

        public T GetComponent<T>() where T : Component
        {
            componentsDictionary.TryGetValue(typeof(T), out Component component);
            return (T)component;
        }

        public List<Component> GetAllComponents()
        {
            return components;
        }
    }
}
