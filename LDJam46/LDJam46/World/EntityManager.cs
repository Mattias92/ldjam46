﻿using LDJam46.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.World
{
    [Serializable()]
    public class EntityManager
    {
        public List<Entity> Entities;
        public int IdCounter = 1;

        public EntityManager()
        {
            Entities = new List<Entity>();
        }

        public int Count
        {
            get
            {
                return Entities.Count;
            }
        }

        public void Add(ContentManager contentManager, Entity template, Vector2 position)
        {
            Entity entity = new Entity(IdCounter++, position, template);

            if (entity.ContainsComponent<Sprite>())
            {
                Sprite sprite = entity.GetComponent<Sprite>();
                sprite.Texture = contentManager.Load<Texture2D>(sprite.TexturePath);
            }

            Entities.Add(entity);
        }

        public void Remove(Vector2 position)
        {
            Entities.RemoveAll(e => e.Position == position);
        }

        public bool Contains(Vector2 position)
        {
            return Entities.Any(e => e.Position == position);
        }

        public void OnLoad(ContentManager contentManager)
        {
            foreach (Entity entity in Entities)
            {
                if (entity.ContainsComponent<Sprite>())
                {
                    Sprite sprite = entity.GetComponent<Sprite>();
                    sprite.Texture = contentManager.Load<Texture2D>(sprite.TexturePath);
                }
            }
        }
    }
}
