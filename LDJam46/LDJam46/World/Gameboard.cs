﻿using LDJam46.Components;
using LDJam46.Physics;
using LDJam46.Systems;
using LDJam46.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.World
{
    [Serializable()]
    public class Gameboard
    {
        [NonSerialized()]
        private ContentManager contentManager;
        public EntityManager EntityManager;
        public Tilemap Tilemap;
        public List<BoxCollider> ColliderCache;
        [NonSerialized()]
        private SpriteFont spriteFont;

        [NonSerialized()]
        private TilesetPalette tilesetPalette;
        [NonSerialized()]
        private EntityPalette entityPalette;

        public string Name;
        public int Width;
        public int Height;
        public bool IsEditing = false;
        public int EggsKilled = 0;

        public bool LevelCleared = false;
        public int LevelClearedCountdown = 30;

        [field: NonSerialized()]
        public event EventHandler<GameboardArgs> OnLevelExit;
        [field: NonSerialized()]
        public event EventHandler<GameboardArgs> OnLevelCleared;

        [NonSerialized()]
        private SpringSetupSystem springSetupSystem;
        [NonSerialized()]
        private ColliderCacheSystem colliderCacheSystem;
        [NonSerialized()]
        private SpringCollisionSystem springCollisionSystem;
        [NonSerialized()]
        private PhysicsSystem physicsSystem;
        [NonSerialized()]
        private TileCollisionSystem tileCollisionSystem;
        [NonSerialized()]
        private EggSpawnerSystem eggSpawnerSystem;
        [NonSerialized()]
        private NestWinSystem nestWinSystem;
        [NonSerialized()]
        private SpriteRenderSystem spriteRenderSystem;
        [NonSerialized()]
        private SpringRenderSystem springRenderSystem;

        [NonSerialized()]
        private MouseState previousMouseState = Mouse.GetState();
        [NonSerialized()]
        private KeyboardState previousKeyboardState = Keyboard.GetState();

        [NonSerialized()]
        Sprite runButtonSprite;
        [NonSerialized()]
        BoxCollider runButtonBoxCollider;

        public Gameboard(ContentManager contentManager, string name, int width, int height)
        {
            this.contentManager = contentManager;
            EntityManager = new EntityManager();
            Tilemap = new Tilemap(contentManager, "grassTiles3", width / Tile.Size, height / Tile.Size);
            ColliderCache = new List<BoxCollider>();
            tilesetPalette = new TilesetPalette(contentManager, "grassTiles3", Vector2.Zero);
            entityPalette = new EntityPalette(contentManager, new Vector2(width - 96, 0));

            Name = name;
            Width = width;
            Height = height;

            EggsKilled = 0;
            LevelCleared = false;
            LevelClearedCountdown = 60;
            spriteFont = contentManager.Load<SpriteFont>("font");

            springSetupSystem = new SpringSetupSystem();
            colliderCacheSystem = new ColliderCacheSystem();
            springCollisionSystem = new SpringCollisionSystem();
            physicsSystem = new PhysicsSystem();
            tileCollisionSystem = new TileCollisionSystem();
            eggSpawnerSystem = new EggSpawnerSystem();
            nestWinSystem = new NestWinSystem();
            spriteRenderSystem = new SpriteRenderSystem();
            springRenderSystem = new SpringRenderSystem(contentManager, "lines", "line2", "arrow2");

            runButtonSprite = new Sprite(contentManager, "run", 128, 128);
            runButtonBoxCollider = new BoxCollider(null, Width - 50 - 128, Height - 50 - 128, 128, 128);
        }

        public void OnLoad(ContentManager contentManager)
        {
            this.contentManager = contentManager;
            ColliderCache = new List<BoxCollider>();
            tilesetPalette = new TilesetPalette(contentManager, "grassTiles3", Vector2.Zero);
            entityPalette = new EntityPalette(contentManager, new Vector2(Width - 96, 0));

            EggsKilled = 0;
            LevelCleared = false;
            LevelClearedCountdown = 30;
            spriteFont = contentManager.Load<SpriteFont>("font");

            springSetupSystem = new SpringSetupSystem();
            colliderCacheSystem = new ColliderCacheSystem();
            springCollisionSystem = new SpringCollisionSystem();
            physicsSystem = new PhysicsSystem();
            tileCollisionSystem = new TileCollisionSystem();
            eggSpawnerSystem = new EggSpawnerSystem();
            nestWinSystem = new NestWinSystem();
            spriteRenderSystem = new SpriteRenderSystem();
            springRenderSystem = new SpringRenderSystem(contentManager, "lines", "line2", "arrow2");

            runButtonSprite = new Sprite(contentManager, "run", 128, 128);
            runButtonBoxCollider = new BoxCollider(null, Width - 50 - 128, Height - 50 - 128, 128, 128);

            EntityManager.OnLoad(contentManager);
            Tilemap.OnLoad(contentManager);
        }

        public void Update(GameTime gameTime)
        {
            MouseState mouseState = Mouse.GetState();
            KeyboardState keyboardState = Keyboard.GetState();

            if (LevelCleared)
            {
                if (LevelClearedCountdown == 0)
                {
                    OnLevelCleared.Invoke(this, new GameboardArgs(Name));
                }
                else
                {
                    LevelClearedCountdown--;
                }
            }

            Vector2 mousePosition = mouseState.Position.ToVector2();
            int tileX = (int)mousePosition.X / Tile.Size;
            int tileY = (int)mousePosition.Y / Tile.Size;

            colliderCacheSystem.Update(this);

            if (keyboardState.IsKeyDown(Keys.R) && previousKeyboardState.IsKeyUp(Keys.R))
            {
                if (!LevelCleared)
                {
                    eggSpawnerSystem.SpawnEgg(contentManager, this);
                }
            }
            else if (keyboardState.IsKeyDown(Keys.Escape) && previousKeyboardState.IsKeyUp(Keys.Escape))
            {
                if (!LevelCleared)
                {
                    OnLevelExit.Invoke(this, new GameboardArgs(Name));
                }
                else
                {
                    LevelClearedCountdown = 0;
                }
            }

            if (IsEditing)
            {
                if (mouseState.LeftButton == ButtonState.Pressed && previousMouseState.LeftButton == ButtonState.Released)
                {
                    if (tilesetPalette.InBounds(mousePosition))
                    {
                        entityPalette.SelectedEntity = null;
                    }
                    else if (entityPalette.InBounds(mousePosition))
                    {
                        tilesetPalette.SelectedTile = null;
                    }
                    else
                    {
                        if (tilesetPalette.SelectedTile != null)
                        {
                            Tilemap.SetTile(tileX, tileY, tilesetPalette.SelectedTile);
                        }
                        else if (entityPalette.SelectedEntity != null)
                        {
                            int x = (int)mousePosition.X / 32 * 32;
                            int y = (int)mousePosition.Y / 32 * 32;

                            if (!EntityManager.Contains(new Vector2(x, y)))
                            {
                                EntityManager.Add(contentManager, entityPalette.SelectedEntity, new Vector2(x, y));
                            }
                        }
                    }
                }
                else if (mouseState.RightButton == ButtonState.Pressed && previousMouseState.RightButton == ButtonState.Released)
                {
                    int x = (int)mousePosition.X / 32 * 32;
                    int y = (int)mousePosition.Y / 32 * 32;

                    EntityManager.Remove(new Vector2(x, y));
                }
            }
            else
            {
                springCollisionSystem.Update(this, gameTime);
                physicsSystem.Update(this, gameTime);
                nestWinSystem.Update(this);
                tileCollisionSystem.Update(this);

                if (mouseState.LeftButton == ButtonState.Pressed && previousMouseState.LeftButton == ButtonState.Released)
                {
                    springSetupSystem.Click(this, mousePosition);

                    if (runButtonBoxCollider.AABB.Contains(mousePosition) && !LevelCleared)
                    {
                        eggSpawnerSystem.SpawnEgg(contentManager, this);
                    }
                }
            }

            List<Entity> EntitiesToRemove = new List<Entity>();

            for (int i = 0; i < EntityManager.Count; i++)
            {
                Entity entity = EntityManager.Entities[i];

                if (entity.IsKilled)
                {
                    EntitiesToRemove.Add(entity);
                }
                else
                {
                    if (entity.X < 0 || entity.X > Width || entity.Y < 0 || entity.Y > Height)
                    {
                        EntitiesToRemove.Add(entity);
                        EggsKilled++;
                    }
                }
            }

            for (int i = 0; i < EntitiesToRemove.Count; i++)
            {
                EntityManager.Entities.Remove(EntitiesToRemove[i]);
            }

            previousMouseState = mouseState;
            previousKeyboardState = keyboardState;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Tilemap.Draw(spriteBatch);

            spriteRenderSystem.Draw(spriteBatch, this);
            springRenderSystem.Draw(spriteBatch, this);

            spriteBatch.Begin();
            string str = "Eggs Smashed: " + EggsKilled;
            Vector2 offset = spriteFont.MeasureString(str);
            spriteBatch.DrawString(spriteFont, "Eggs Smashed: " + EggsKilled, new Vector2(Width / 2 - offset.X / 2, 0), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);

            spriteBatch.Draw(runButtonSprite.Texture, new Vector2(runButtonBoxCollider.AABB.X, runButtonBoxCollider.AABB.Y), Color.Green);
            spriteBatch.End();

            if (IsEditing)
            {
                tilesetPalette.Draw(spriteBatch);
                entityPalette.Draw(spriteBatch);
            }
        }
    }
}
