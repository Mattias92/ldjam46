﻿using LDJam46.Components;
using LDJam46.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Utility
{
    public class EntityPalette
    {
        public Entity SelectedEntity;
        public bool IsActive;

        private Entity[][] entities;
        public Vector2 Position;
        public Vector2 Offset;

        private int width;
        private int height;

        public EntityPalette(ContentManager contentManager, Vector2 position)
        {
            Position = position;
            Offset = new Vector2(0, 0);

            int xi = 0;
            int yi = 0;

            Entity springEntity = new Entity(0, new Vector2(Tile.Size * xi + Offset.X, Tile.Size * yi + Offset.Y));
            Spring spring = new Spring(springEntity, new Vector2(1f, 0f), 64);
            BoxCollider springBoxCollider = new BoxCollider(springEntity, Tile.Size * xi + Offset.X, Tile.Size * yi + Offset.Y, Tile.Size, Tile.Size);
            Sprite springSprite = new Sprite(contentManager, "springbase2", 32, 32);
            springEntity.AddComponent(spring);
            springEntity.AddComponent(springBoxCollider);
            springEntity.AddComponent(springSprite);
            xi++;

            Entity eggSpawner = new Entity(0, new Vector2(Tile.Size * xi + Offset.X, Tile.Size * yi + Offset.Y));
            Spawner spawner = new Spawner();
            Sprite eggSpawnerSprite = new Sprite(contentManager, "eggpipe", 32, 32);
            eggSpawner.AddComponent(spawner);
            eggSpawner.AddComponent(eggSpawnerSprite);
            xi++;

            Entity nestEntity = new Entity(0, new Vector2(Tile.Size * xi + Offset.X, Tile.Size * yi + Offset.Y));
            Sprite nestSprite = new Sprite(contentManager, "nest", 96, 32);
            BoxCollider nestBoxCollider = new BoxCollider(nestEntity, Tile.Size * xi + Offset.X, Tile.Size * yi + Offset.Y, 96, 32);
            Nest nest = new Nest();
            nestEntity.AddComponent(nestSprite);
            nestEntity.AddComponent(nestBoxCollider);
            nestEntity.AddComponent(nest);

            entities = new Entity[][]
            {
                new Entity[]
                {
                    springEntity,
                    eggSpawner,
                    nestEntity
                }
            };

            width = 96;
            height = 32;
        }

        public bool InBounds(Vector2 cursorPosition)
        {
            Rectangle bounds = new Rectangle((int)(Position.X - Offset.X), (int)(Position.Y - Offset.Y), width, height);

            if (bounds.Contains(cursorPosition))
            {
                for (int y = 0; y < entities.Length; y++)
                {
                    for (int x = 0; x < entities[y].Length; x++)
                    {
                        int x1 = x * Tile.Size + (int)(Position.X - Offset.X);
                        int x2 = x * Tile.Size + Tile.Size + (int)(Position.X - Offset.X);

                        int y1 = y * Tile.Size + (int)(Position.Y - Offset.Y);
                        int y2 = y * Tile.Size + Tile.Size + (int)(Position.Y - Offset.Y);

                        if (cursorPosition.X >= x1 && cursorPosition.X < x2 && cursorPosition.Y >= y1 && cursorPosition.Y < y2)
                        {
                            SelectedEntity = entities[y][x];
                            return true;
                        }
                    }
                }

                return true;
            }

            return false;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            for (int y = 0; y < entities.Length; y++)
            {
                for (int x = 0; x < entities[y].Length; x++)
                {
                    Entity entity = entities[y][x];
                    
                    if (entity.ContainsComponent<Sprite>())
                    {
                        Sprite sprite = entity.GetComponent<Sprite>();
                        spriteBatch.Draw(sprite.Texture, Position - Offset + new Vector2(x * Tile.Size, y * Tile.Size), new Rectangle(0 * Tile.Size, 0 * Tile.Size, Tile.Size, Tile.Size), Color.White);
                    }
                }
            }
            spriteBatch.End();
        }
    }
}
