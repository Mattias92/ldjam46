﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Utility
{
    public class GameboardArgs : EventArgs
    {
        public string LevelName;

        public GameboardArgs(string levelName)
        {
            LevelName = levelName;
        }
    }
}
