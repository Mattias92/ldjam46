﻿using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Utility
{
    public static class Sfx
    {
        public static Dictionary<string, SoundEffect> SoundEffects = new Dictionary<string, SoundEffect>();

        public static void PlaySound(string effect, float volume = 1f)
        {
            if (SoundEffects.ContainsKey(effect))
            {
                SoundEffects[effect].Play(volume, 0f, 0f);
            }
        }
    }
}
