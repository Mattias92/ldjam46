﻿using LDJam46.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Utility
{
    public class TilesetPalette
    {
        public Tile SelectedTile;
        public bool IsActive;

        private Tile[][] tiles;
        private Texture2D texture;
        private Vector2 position;
        private Vector2 offset;
        private int width;
        private int height;

        public TilesetPalette(ContentManager contentManager, string atlasPath, Vector2 position)
        {
            texture = contentManager.Load<Texture2D>(atlasPath);
            this.position = position;
            width = texture.Width;
            height = texture.Height;
            offset = new Vector2(0, 0);

            tiles = new Tile[][]
            {
                new Tile[]
                {
                    new Tile(false, 0, 0),
                    new Tile(true, 1, 0),
                    new Tile(true, 2, 0),

                },
                new Tile[]
                {
                    new Tile(true, 0, 1),
                    new Tile(true, 1, 1),
                    new Tile(true, 2, 1),
                },
                new Tile[]
                {
                    new Tile(true, 0, 2),
                    new Tile(true, 1, 2),
                    new Tile(true, 2, 2),
                },
            };
        }

        public bool InBounds(Vector2 cursorPosition)
        {
            Rectangle bounds = new Rectangle((int)(position.X - offset.X), (int)position.Y, width, height);

            if (bounds.Contains(cursorPosition))
            {
                for (int y = 0; y < tiles.Length; y++)
                {
                    for (int x = 0; x < tiles[y].Length; x++)
                    {
                        int x1 = x * Tile.Size + (int)(position.X - offset.X);
                        int x2 = x * Tile.Size + Tile.Size + (int)(position.X - offset.X);

                        int y1 = y * Tile.Size + (int)(position.Y - offset.Y);
                        int y2 = y * Tile.Size + Tile.Size + (int)(position.Y - offset.Y);

                        if (cursorPosition.X >= x1 && cursorPosition.X < x2 && cursorPosition.Y >= y1 && cursorPosition.Y < y2)
                        {
                            SelectedTile = tiles[y][x];
                            return true;
                        }
                    }
                }

                return true;
            }

            return false;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            for (int y = 0; y < tiles.Length; y++)
            {
                for (int x = 0; x < tiles[y].Length; x++)
                {
                    Tile tile = tiles[y][x];
                    spriteBatch.Draw(texture, position - offset + new Vector2(x * Tile.Size, y * Tile.Size), new Rectangle(tile.U * Tile.Size, tile.V * Tile.Size, Tile.Size, Tile.Size), Color.White);
                }
            }
            spriteBatch.End();
        }
    }
}
