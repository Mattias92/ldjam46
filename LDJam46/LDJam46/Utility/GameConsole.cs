﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LDJam46.Utility
{
    public class GameConsole
    {
        public event EventHandler<GameboardArgs> OnLevelLoad;
        public event EventHandler<GameboardArgs> OnLevelSave;
        public event EventHandler<GameboardArgs> OnLevelCreate;
        public event EventHandler<GameboardArgs> OnLevelEdit;

        private readonly GraphicsDevice graphicsDevice;
        private readonly SpriteFont spriteFont;
        private readonly Texture2D panel;
        private RasterizerState rasterizerState;

        public bool IsActive;
        public int Width;
        public int Height;
        public float LineSpacing;

        private List<string> previousLines = new List<string>();
        private string inputLine = "";

        public GameConsole(GraphicsDevice graphicsDevice, ContentManager contentManager)
        {
            this.graphicsDevice = graphicsDevice;
            spriteFont = contentManager.Load<SpriteFont>("Font");

            IsActive = false;
            Width = graphicsDevice.Viewport.Width;
            Height = graphicsDevice.Viewport.Height / 3;
            LineSpacing = spriteFont.LineSpacing;

            panel = new Texture2D(graphicsDevice, Width, Height);
            Color[] data = new Color[(Width) * Height];
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = new Color(Color.Black, 0.5f);
            }
            panel.SetData(data);

            rasterizerState = new RasterizerState
            {
                MultiSampleAntiAlias = true,
                ScissorTestEnable = true,
                FillMode = FillMode.Solid,
                CullMode = CullMode.CullCounterClockwiseFace,
                DepthBias = 0,
                SlopeScaleDepthBias = 0
            };
        }

        public void TextInputHandler(object sender, TextInputEventArgs args)
        {
            if (!IsActive)
            {
                return;
            }

            char character = args.Character;
            Keys key = args.Key;

            if (key == Keys.Enter)
            {
                if (inputLine.Length > 0)
                {
                    string[] parts = inputLine.Split(' ');
                    if (parts[0].ToLower() == "save")
                    {
                        if (parts.Count() > 1)
                        {
                            string levelName = parts[1];
                            OnLevelSave.Invoke(this, new GameboardArgs(levelName));
                            previousLines.Add("Saved!");
                        }
                        else
                        {
                            OnLevelSave.Invoke(this, new GameboardArgs(""));
                            previousLines.Add("Saved!");
                        }
                    }
                    else if (parts[0].ToLower() == "load")
                    {
                        if (parts.Count() > 1)
                        {
                            string levelName = parts[1];
                            OnLevelLoad.Invoke(this, new GameboardArgs(levelName));
                            previousLines.Add("Loaded level: " + levelName + "!");
                        }
                    }
                    else if (parts[0].ToLower() == "create")
                    {
                        if (parts.Count() > 1)
                        {
                            string levelName = parts[1];

                            if (Regex.IsMatch(levelName, @"^[a-zA-Z0-9]+$"))
                            {
                                OnLevelCreate?.Invoke(this, new GameboardArgs(levelName));
                                previousLines.Add("Created new level: " + levelName + "!");
                            }
                            else
                            {
                                previousLines.Add("Invalid level name!");
                            }
                        }
                        else
                        {
                            previousLines.Add("Create level requires a name!");
                        }
                    }
                    else if (parts[0].ToLower() == "edit")
                    {
                        OnLevelEdit.Invoke(this, new GameboardArgs(""));
                    }

                    inputLine = "";
                }
            }
            else if (key == Keys.Back)
            {
                if (inputLine.Length > 0)
                {
                    inputLine = inputLine.Remove(inputLine.Length - 1);
                }
            }
            else
            {
                if (Regex.IsMatch(character.ToString(), @"^[a-zA-Z0-9 ]+$"))
                {
                    inputLine += character;
                }   
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (!IsActive)
            {
                return;
            }

            graphicsDevice.ScissorRectangle = new Rectangle(0, 0, Width, Height);
            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, rasterizerState, null, null);

            spriteBatch.Draw(panel, Vector2.Zero, Color.White);

            string inputMarker = ">";
            float inputWidth = spriteFont.MeasureString(inputMarker).X + spriteFont.MeasureString(inputLine).X;
            float inputOverflow = 0f;
            if (Width - inputWidth < 0)
            {
                inputOverflow = Width - inputWidth;
            }

            spriteBatch.DrawString(spriteFont, inputMarker + inputLine, new Vector2(inputOverflow, Height - LineSpacing), Color.White);

            for (int i = 0; i < previousLines.Count; i++)
            {
                spriteBatch.DrawString(spriteFont, previousLines[i], new Vector2(0, Height - LineSpacing - (previousLines.Count * LineSpacing) + i * LineSpacing), Color.White);
            }
            spriteBatch.End();
        }
    }
}
