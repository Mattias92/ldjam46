﻿using LDJam46.Components;
using LDJam46.Utility;
using LDJam46.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;

namespace LDJam46
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private SpriteFont titleFont;

        private Gameboard gameboard;
        private GameConsole gameConsole;

        private KeyboardState previousKeyboardState = Keyboard.GetState();
        private MouseState previousMouseState = Mouse.GetState();
        private string assemblyDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

        private List<Entity> levels;
        private Entity currentLevel;
        private int framesSinceExit = 0;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            graphics.PreferredBackBufferHeight = 704;
            graphics.PreferredBackBufferWidth = 1280;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            Window.Title = "Save The Egg";
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            gameConsole = new GameConsole(GraphicsDevice, Content);
            Window.TextInput += gameConsole.TextInputHandler;
            gameConsole.OnLevelLoad += Load;
            gameConsole.OnLevelSave += Save;
            gameConsole.OnLevelCreate += Create;
            gameConsole.OnLevelEdit += EditLevel;
            titleFont = Content.Load<SpriteFont>("title");

            levels = new List<Entity>();
            Entity level1 = new Entity(0, new Vector2(1280 / 3 - 128, 704 / 2 - 128 / 2));
            Entity level2 = new Entity(0, new Vector2((1280 / 3 - 128) * 2, 704 / 2 - 128 / 2));
            Entity level3 = new Entity(0, new Vector2((1280 / 3 - 128) * 3, 704 / 2 - 128 / 2));
            Sprite sprite1 = new Sprite(Content, "levelselect", 128, 128);
            sprite1.Color = Color.White;
            Sprite sprite2 = new Sprite(Content, "levelselect", 128, 128);
            sprite2.Color = Color.White;
            Sprite sprite3 = new Sprite(Content, "levelselect", 128, 128);
            sprite3.Color = Color.White;
            BoxCollider boxCollider1 = new BoxCollider(level1, level1.X, level1.Y, 128, 128);
            BoxCollider boxCollider2 = new BoxCollider(level2, level2.X, level2.Y, 128, 128);
            BoxCollider boxCollider3 = new BoxCollider(level3, level3.X, level3.Y, 128, 128);
            Level levelPath1 = new Level("level1");
            Level levelPath2 = new Level("level2");
            Level levelPath3 = new Level("level3");
            level1.AddComponent(sprite1);
            level1.AddComponent(boxCollider1);
            level1.AddComponent(levelPath1);
            level2.AddComponent(sprite2);
            level2.AddComponent(boxCollider2);
            level2.AddComponent(levelPath2);
            level3.AddComponent(sprite3);
            level3.AddComponent(boxCollider3);
            level3.AddComponent(levelPath3);
            levels.Add(level1);
            levels.Add(level2);
            levels.Add(level3);

            Sfx.SoundEffects.Add("win2", Content.Load<SoundEffect>("win2"));
            Sfx.SoundEffects.Add("springsound", Content.Load<SoundEffect>("springsound"));
            Sfx.SoundEffects.Add("eggcrash", Content.Load<SoundEffect>("eggcrash"));
        }

        public void Load(object sender, GameboardArgs args)
        {
            string dir = Path.Combine(assemblyDirectory, Content.RootDirectory + "/Levels/" + args.LevelName + ".bin");

            if (File.Exists(dir))
            {
                Stream openFileStream = File.OpenRead(dir);
                BinaryFormatter deserializer = new BinaryFormatter();
                gameboard = (Gameboard)deserializer.Deserialize(openFileStream);
                openFileStream.Close();
            }
            gameboard.OnLoad(Content);
            gameboard.OnLevelCleared += LevelClear;
            gameboard.OnLevelExit += LevelExit;
        }

        public void Save(object sender, GameboardArgs args)
        {
            string dir = Path.Combine(assemblyDirectory, Content.RootDirectory + "/Levels");

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            string levelName = args.LevelName;
            if (levelName == "")
            {
                levelName = gameboard.Name;
            }

            Stream SaveFileStream = File.Create(Path.Combine(dir, levelName + ".bin"));
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(SaveFileStream, gameboard);
            SaveFileStream.Close();
        }

        public void Create(object sender, GameboardArgs args)
        {
            gameboard = new Gameboard(Content, args.LevelName, 1280, 704);
        }

        public void LevelExit(object sender, GameboardArgs args)
        {
            gameboard = null;
            framesSinceExit = 10;
        }

        public void LevelClear(object sender, GameboardArgs args)
        {
            if (currentLevel != null)
            {
                currentLevel.GetComponent<Sprite>().Color = Color.Green;
            }
            gameboard = null;
        }

        public void EditLevel(object sender, GameboardArgs args)
        {
            if (gameboard != null)
            {
                gameboard.IsEditing = !gameboard.IsEditing;
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();
            MouseState mouseState = Mouse.GetState();
            Vector2 mousePosition = mouseState.Position.ToVector2();

            if (keyboardState.IsKeyDown(Keys.F1) && previousKeyboardState.IsKeyUp(Keys.F1))
            {
                gameConsole.IsActive = !gameConsole.IsActive;
            }

            base.Update(gameTime);

            if (!gameConsole.IsActive)
            {
                if (gameboard != null)
                {
                    gameboard.Update(gameTime);
                }
                else
                {
                    if (mouseState.LeftButton == ButtonState.Pressed && previousMouseState.LeftButton == ButtonState.Released)
                    {
                        for (int i = 0; i < levels.Count; i++)
                        {
                            Entity entity = levels[i];
                            BoxCollider boxCollider = entity.GetComponent<BoxCollider>();
                            if (boxCollider.AABB.Contains(mousePosition))
                            {
                                Level level = entity.GetComponent<Level>();
                                currentLevel = entity;
                                Load(this, new GameboardArgs(level.LevelName));
                                break;
                            }
                        }
                    }
                }
                
            }

            if (keyboardState.IsKeyDown(Keys.Escape) && previousKeyboardState.IsKeyUp(Keys.Escape) && framesSinceExit == 0)
            {
                Exit();
            }

            previousMouseState = mouseState;
            previousKeyboardState = keyboardState;
            if (framesSinceExit > 0)
            {
                framesSinceExit--;
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            base.Draw(gameTime);

            if (gameboard != null)
            {
                gameboard.Draw(spriteBatch);
            }
            else
            {
                spriteBatch.Begin();
                string str = "Save The Egg!";
                Vector2 offset = titleFont.MeasureString(str);
                spriteBatch.DrawString(titleFont, str, new Vector2(1280 / 2f - offset.X / 2f, 75f), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                for (int i = 0; i < levels.Count; i++)
                {
                    Entity entity = levels[i];
                    Sprite sprite = entity.GetComponent<Sprite>();
                    spriteBatch.Draw(sprite.Texture, entity.Position, new Rectangle(i * 128, 0, 128, 128), sprite.Color);
                }
                spriteBatch.End();
            }
            gameConsole.Draw(spriteBatch);
        }
    }
}
