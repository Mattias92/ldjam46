﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Physics
{
    [Serializable()]
    public class AABB : IEquatable<AABB>
    {
        public float X;
        public float Y;
        public float Width;
        public float Height;

        public Vector2 Min
        {
            get
            {
                return new Vector2(X, Y);
            }
        }

        public Vector2 Max
        {
            get
            {
                return new Vector2(X + Width, Y + Height);
            }
        }

        public float Left
        {
            get
            {
                return X;
            }
        }

        public float Right
        {
            get
            {
                return X + Width;
            }
        }

        public float Top
        {
            get
            {
                return Y;
            }
        }

        public float Bottom
        {
            get
            {
                return Y + Height;
            }
        }

        public Vector2 Center
        {
            get
            {
                return new Vector2(X + Width / 2, Y + Height / 2);
            }
        }

        public Vector2 Halfsize
        {
            get
            {
                return new Vector2(Width / 2, Height / 2);
            }
        }

        public AABB(float x, float y, float width, float height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public AABB(AABB template)
        {
            X = template.X;
            Y = template.Y;
            Width = template.Width;
            Height = template.Height;
        }

        public bool Intersects(AABB other)
        {
            Vector2 c = Center - other.Center;

            double ex = Math.Abs(c.X) - (Halfsize.X + other.Halfsize.X);
            double ey = Math.Abs(c.Y) - (Halfsize.Y + other.Halfsize.Y);

            return (ex < 0) && (ey < 0);
        }

        public bool Contains(Vector2 position)
        {
            return position.X >= Left && position.X <= Right && position.Y >= Top && position.Y <= Bottom;
        }

        public override bool Equals(object obj)
        {
            return (obj is AABB) ? Equals((AABB)obj) : false;
        }

        public bool Equals(AABB other)
        {
            return Min.Equals(other.Min) && Max.Equals(other.Max);
        }

        public override int GetHashCode()
        {
            return Min.GetHashCode() ^ Max.GetHashCode();
        }
    }
}
