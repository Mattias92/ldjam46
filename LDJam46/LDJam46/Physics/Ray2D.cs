﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Physics
{
    public class Ray2D : IEquatable<Ray2D>
    {
        public Vector2 Position;
        public Vector2 Direction;

        public Ray2D(Vector2 position, Vector2 direction)
        {
            Position = position;
            Direction = direction;
        }

        public float? Intersects(AABB aabb)
        {
            // Check in ray starts off in rect.
            if (Position.X >= aabb.Left
                && Position.X <= aabb.Right
                && Position.Y >= aabb.Top
                && Position.Y <= aabb.Bottom)
            {
                return 0f;
            }

            Vector2 maxT = new Vector2(-1f);

            if (Position.X < aabb.Left && Direction.X != 0f)
            {
                maxT.X = (aabb.Left - Position.X) / Direction.X;
            }
            else if (Position.X > aabb.Right && Direction.X != 0f)
            {
                maxT.X = (aabb.Right - Position.X) / Direction.X;
            }

            if (Position.Y < aabb.Top && Direction.Y != 0f)
            {
                maxT.Y = (aabb.Top - Position.Y) / Direction.Y;
            }
            else if (Position.Y > aabb.Bottom && Direction.Y != 0f)
            {
                maxT.Y = (aabb.Bottom - Position.Y) / Direction.Y;
            }

            if (maxT.X > maxT.Y)
            {
                if (maxT.X < 0f)
                {
                    return null;
                }

                float coord = Position.Y + maxT.X * Direction.Y;
                if (coord < aabb.Top || coord > aabb.Bottom)
                {
                    return null;
                }

                return maxT.X;
            }
            else
            {
                if (maxT.Y < 0f)
                {
                    return null;
                }

                float coord = Position.X + maxT.Y * Direction.X;
                if (coord < aabb.Left || coord > aabb.Right)
                {
                    return null;
                }

                return maxT.Y;
            }
        }

        public override bool Equals(object obj)
        {
            return (obj is Ray) ? Equals((Ray)obj) : false;
        }

        public bool Equals(Ray2D other)
        {
            return Position.Equals(other.Position) && Direction.Equals(other.Direction);
        }

        public override int GetHashCode()
        {
            return Position.GetHashCode() ^ Direction.GetHashCode();
        }
    }
}
