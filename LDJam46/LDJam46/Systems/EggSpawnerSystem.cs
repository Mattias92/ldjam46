﻿using LDJam46.Components;
using LDJam46.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Systems
{
    public class EggSpawnerSystem
    {
        public void SpawnEgg(ContentManager contentManager, Gameboard gameboard)
        {
            EntityManager entityManager = gameboard.EntityManager;

            for (int i = 0; i < entityManager.Count; i++)
            {
                Entity entity = entityManager.Entities[i];

                if (entity.ContainsComponent<Spawner>())
                {
                    Entity eggEntity = new Entity(entityManager.IdCounter++, entity.Position + new Vector2(16f, 16f));
                    Sprite sprite = new Sprite(contentManager, "egg2", 16, 16);
                    Vector2 origin = new Vector2(sprite.Width / 2, sprite.Height / 2);
                    sprite.Origin = origin;
                    Velocity velocity = new Velocity();
                    BoxCollider boxCollider = new BoxCollider(eggEntity, entity.Position.X + 8f, entity.Position.Y + 8f, 16, 16);
                    Egg egg = new Egg();
                    eggEntity.AddComponent(sprite);
                    eggEntity.AddComponent(velocity);
                    eggEntity.AddComponent(boxCollider);
                    eggEntity.AddComponent(egg);
                    entityManager.Entities.Add(eggEntity);
                }
            }
        }
    }
}
