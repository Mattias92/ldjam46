﻿using LDJam46.Components;
using LDJam46.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Systems
{
    public class SpriteRenderSystem
    {
        public SpriteRenderSystem()
        {

        }

        public void Draw(SpriteBatch spriteBatch, Gameboard gameboard)
        {
            EntityManager entityManager = gameboard.EntityManager;
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);

            for (int i = 0; i < entityManager.Count; i++)
            {
                Entity entity = entityManager.Entities[i];

                if (entity.ContainsComponent<Sprite>())
                {
                    Sprite sprite = entity.GetComponent<Sprite>();
                    spriteBatch.Draw(sprite.Texture, new Rectangle((int)entity.X, (int)entity.Y, sprite.Width, sprite.Height), null, Color.White, sprite.Rotation, sprite.Origin, SpriteEffects.None, sprite.Order);
                }
            }

            spriteBatch.End();
        }
    }
}
