﻿using LDJam46.Components;
using LDJam46.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Systems
{
    public class ColliderCacheSystem
    {
        public void Update(Gameboard gameboard)
        {
            EntityManager entityManager = gameboard.EntityManager;
            gameboard.ColliderCache.Clear();

            for (int i = 0; i < entityManager.Count; i++)
            {
                Entity entity = entityManager.Entities[i];

                if (entity.ContainsComponent<BoxCollider>())
                {
                    BoxCollider collider = entity.GetComponent<BoxCollider>();
                    collider.AABB.X = entity.X;
                    collider.AABB.Y = entity.Y;
                    gameboard.ColliderCache.Add(collider);
                }
            }
        }
    }
}
