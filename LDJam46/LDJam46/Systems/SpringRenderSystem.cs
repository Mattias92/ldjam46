﻿using LDJam46.Components;
using LDJam46.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Systems
{
    public class SpringRenderSystem
    {
        private Texture2D textureArrow;
        private Texture2D textureAnchor;
        private Texture2D textureLine;

        public SpringRenderSystem(ContentManager contentManager, string textureAnchorPath, string textureLinePath, string textureArrowPath)
        {
            textureArrow = contentManager.Load<Texture2D>(textureArrowPath);
            textureAnchor = contentManager.Load<Texture2D>(textureAnchorPath);
            textureLine = contentManager.Load<Texture2D>(textureLinePath);
        }

        public void Draw(SpriteBatch spriteBatch, Gameboard gameboard)
        {
            EntityManager entityManager = gameboard.EntityManager;
            spriteBatch.Begin();

            for (int i = 0; i < entityManager.Count; i++)
            {
                Entity entity = entityManager.Entities[i];

                if (entity.ContainsComponent<Spring>())
                {
                    Spring spring = entity.GetComponent<Spring>();

                    Vector2 start = entity.Position + new Vector2(16f, 16f);
                    Vector2 end = new Vector2(spring.Direction.X * spring.Distance, spring.Direction.Y * spring.Distance) + start;
                    Vector2 center = entity.Position + new Vector2(16, 16);
                    Vector2 v = Vector2.Normalize(start - end);

                    float angle = (float)Math.Acos(Vector2.Dot(v, -Vector2.UnitX));
                    if (start.Y > end.Y)
                    {
                        angle = MathHelper.TwoPi - angle;
                    }

                    spriteBatch.Draw(textureLine, new Rectangle((int)center.X, (int)center.Y, 8, (int)(end - start).Length()), 
                        null, Color.Yellow, angle + MathHelper.PiOver2, new Vector2(4f, 8f), SpriteEffects.None, 0f);
                    spriteBatch.Draw(textureAnchor, start - new Vector2(4, 4), Color.Yellow);
                    spriteBatch.Draw(textureArrow, end, null, Color.Yellow, angle + MathHelper.PiOver2, new Vector2(4f, 7f), 1f, SpriteEffects.None, 0f);
                }
            }

            spriteBatch.End();
        }
    }
}
