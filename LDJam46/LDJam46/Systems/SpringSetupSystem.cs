﻿using LDJam46.Components;
using LDJam46.Physics;
using LDJam46.World;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Systems
{
    public class SpringSetupSystem
    {
        private bool hasSelectedSpring = false;
        private Spring selectedSpring;

        public void Click(Gameboard gameboard, Vector2 position)
        {
            if (hasSelectedSpring)
            {
                Vector2 center = selectedSpring.Owner.Position + new Vector2(16, 16);
                Vector2 diffrence = Vector2.Subtract(position, center);

                selectedSpring.Distance = Vector2.Distance(center, position);
                selectedSpring.Direction = Vector2.Normalize(diffrence);

                hasSelectedSpring = false;
            }
            else
            {
                for (int i = 0; i < gameboard.ColliderCache.Count; i++)
                {
                    BoxCollider boxCollider = gameboard.ColliderCache[i];

                    if (boxCollider.Owner.ContainsComponent<Spring>() && boxCollider.AABB.Contains(position))
                    {
                        selectedSpring = boxCollider.Owner.GetComponent<Spring>();
                        hasSelectedSpring = true;
                        break;
                    }
                }
            }
        }
    }
}
