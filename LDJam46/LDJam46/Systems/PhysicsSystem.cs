﻿using LDJam46.Components;
using LDJam46.World;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Systems
{
    public class PhysicsSystem
    {
        public void Update(Gameboard gameboard, GameTime gameTime)
        {
            EntityManager entityManager = gameboard.EntityManager;

            for (int i = 0; i < entityManager.Count; i++)
            {
                Entity entity = entityManager.Entities[i];

                if (entity.ContainsComponent<Velocity>())
                {
                    Velocity velocity = entity.GetComponent<Velocity>();

                    float delta = (float)gameTime.ElapsedGameTime.TotalSeconds;

                    if (!velocity.GotHitByRay)
                    {
                        velocity.HasGravity = true;
                    }

                    float gravity = 300f;
                    if (velocity.HasGravity)
                    {
                        velocity.Y += gravity * delta;
                    }
                    entity.Y += velocity.Y * delta;

                    entity.X += velocity.X * delta;
                    if (velocity.HasGravity)
                    {
                        if (velocity.X > 0)
                        {
                            velocity.X += -2.5f;
                            if (velocity.X < 0)
                            {
                                velocity.X = 0;
                            }
                        }
                        else if (velocity.X < 0)
                        {
                            velocity.X += 2.5f;
                            if (velocity.X > 0)
                            {
                                velocity.X = 0;
                            }
                        }
                    }

                    velocity.GotHitByRay = false;
                }
            }
        }
    }
}
