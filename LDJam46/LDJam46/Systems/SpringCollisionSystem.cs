﻿using LDJam46.Components;
using LDJam46.Physics;
using LDJam46.Utility;
using LDJam46.World;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Systems
{
    public class SpringCollisionSystem
    {
        public void Update(Gameboard gameboard, GameTime gameTime)
        {
            EntityManager entityManager = gameboard.EntityManager;

            for (int i = 0; i < entityManager.Count; i++)
            {
                Entity entity = entityManager.Entities[i];

                if (entity.ContainsComponent<Spring>())
                {
                    Spring spring = entity.GetComponent<Spring>();
                    
                    if (spring.RechargeTimer > 0)
                    {
                        spring.RechargeTimer--;
                        continue;
                    }
                    
                    Ray2D ray = new Ray2D(entity.Position + new Vector2(12f, 12f), spring.Direction);

                    for (int j = 0; j < gameboard.ColliderCache.Count; j++)
                    {
                        float? rayResult = ray.Intersects(gameboard.ColliderCache[j].AABB);
                        Entity colliderEntity = gameboard.ColliderCache[j].Owner;

                        if (!colliderEntity.ContainsComponent<Velocity>())
                        {
                            continue;
                        }

                        Velocity velocity = colliderEntity.GetComponent<Velocity>();

                        if (rayResult.HasValue && rayResult > 0 && rayResult <= spring.Distance)
                        {
                            velocity.GotHitByRay = true;
                            float delta = (float)gameTime.ElapsedGameTime.TotalSeconds;

                            if (velocity.HasGravity)
                            {
                                velocity.Y = 0;
                                velocity.X = 0;
                                Sfx.PlaySound("springsound", 0.5f);
                            }

                            velocity.X += spring.Direction.X * 2000f * delta;
                            velocity.Y += spring.Direction.Y * 2000f * delta;

                            velocity.HasGravity = false;
                        }
                    }
                }
            }
        }
    }
}
