﻿using LDJam46.Components;
using LDJam46.Physics;
using LDJam46.Utility;
using LDJam46.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Systems
{
    public class TileCollisionSystem
    {
        public void Update(Gameboard gameboard)
        {
            EntityManager entityManager = gameboard.EntityManager;

            for (int i = 0; i < entityManager.Count; i++)
            {
                Entity entity = entityManager.Entities[i];

                if (entity.ContainsComponent<BoxCollider>() && entity.ContainsComponent<Egg>())
                {
                    BoxCollider boxCollider = entity.GetComponent<BoxCollider>();
                    AABB aabb = boxCollider.AABB;

                    int left = (int)Math.Floor(aabb.Left / Tile.Size);
                    int top = (int)Math.Floor(aabb.Top / Tile.Size);
                    int right = (int)Math.Ceiling(aabb.Right / Tile.Size) - 1;
                    int bottom = (int)Math.Ceiling(aabb.Bottom / Tile.Size) - 1;

                    for (int x = left; x <= right; x++)
                    {
                        for (int y = top; y <= bottom; y++)
                        {
                            if (gameboard.Tilemap.IsCollidable(x, y))
                            {
                                if (!gameboard.LevelCleared)
                                {
                                    gameboard.EggsKilled++;
                                    Sfx.PlaySound("eggcrash", 1f);
                                }
                                entity.IsKilled = true;
                            }
                        }
                    }
                }
            }
        }
    }
}
