﻿using LDJam46.Components;
using LDJam46.Physics;
using LDJam46.Utility;
using LDJam46.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam46.Systems
{
    public class NestWinSystem
    {
        public void Update(Gameboard gameboard)
        {
            foreach (BoxCollider collider1 in gameboard.ColliderCache)
            {
                foreach (BoxCollider collider2 in gameboard.ColliderCache)
                {
                    if (collider1.Owner.Id == collider2.Owner.Id)
                    {
                        continue;
                    }

                    if (collider1.AABB.Intersects(collider2.AABB))
                    {
                        if (collider1.Owner.ContainsComponent<Egg>() && collider2.Owner.ContainsComponent<Nest>())
                        {
                            if (!gameboard.LevelCleared)
                            {
                                Sfx.PlaySound("win2", 1f);
                                gameboard.LevelCleared = true;
                            }
                        }
                        else if (collider2.Owner.ContainsComponent<Egg>() && collider1.Owner.ContainsComponent<Nest>())
                        {
                            if (!gameboard.LevelCleared)
                            {
                                Sfx.PlaySound("win2", 0.5f);
                                gameboard.LevelCleared = true;
                            }   
                        }
                    }
                }
            }
        }
    }
}
